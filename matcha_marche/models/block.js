var db = require('../config/connection');

exports.create = function(userId, targetId) {
    db.query('INSERT INTO blocks SET userId = ?, targetId = ?', [userId, targetId], function(err) {
        if (err) throw err
        else {
            console.log('Block with success!');
        }
    })
}

exports.delete = function(userId, targetId) {
    db.query('DELETE FROM blocks WHERE userId = ? AND targetId = ?', [userId, targetId], function(err) {
        if (err) throw err
        else {
            console.log('Unblock with success!');
        }
    })
}

exports.checkIfBlocked = function(userId, targetId, callback) {
    db.query('SELECT * FROM blocks WHERE userId = ? AND targetId = ?', [userId, targetId], function(err, rows) {
        if (err) throw err;
        callback (rows);
    })
}