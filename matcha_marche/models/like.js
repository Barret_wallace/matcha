var db = require('../config/connection');

exports.create = function(userId, targetId) {
    db.query('INSERT INTO likes SET userId = ?, targetId = ?', [userId, targetId], function(err) {
        if (err) throw err
        else {
            console.log('Like with success!');
        }
    })
}

exports.checkIfLiked = function(userId, targetId, callback) {
    db.query('SELECT * FROM likes WHERE userId = ? AND targetId = ?', [userId, targetId], function(err, rows) {
        if (err) throw err;
        callback (rows);
    })
}

exports.checkLikeMe = function(userId, targetId, callback) {
    db.query('SELECT * FROM likes WHERE userId = ? AND targetId = ?', [userId, targetId], function(err, rows) {
        if (err) throw err;
        callback (rows);
    })
}

exports.delete = function(userId, targetId) {
    db.query('DELETE FROM likes WHERE userId = ? AND targetId = ?', [userId, targetId], function(err) {
        if (err) throw err
        else {
            console.log('Unlike with success!');
        }
    })
}