var express = require('express');
var router = express.Router();
var session = require('express-session');
var User = require('../models/user');
var Image = require('../models/image');
var Tag = require('../models/tag');
var Like = require('../models/like');
var Block = require('../models/block');
var fs = require('fs-extra');
var fileUpload = require('express-fileupload');
var async = require('async');
//mail require
require('dotenv').config();
const nodemailer = require('nodemailer');
// var io = require('socket.io');

router.get('/', function (req, res) {
    res.render('notFound', { layout: 'dashboard' });
})

router.get('/:username', function (req, res) {
    User.getByusername(req.params.username, function (user) {
        if (user != undefined) {
            req.session.targetId = user.userId;
            var url = 'http://localhost:5000/profile/' + req.params.username;
            Image.getAllByUserId(user.userId, function (images) {
                Image.getMain(user.userId, function (mainpix) {
                    Tag.getMine(user.userId, function (tags) {
                        Like.checkIfLiked(req.session.userId, user.userId, function(likeRows) {
                            if (likeRows.length > 0)
                                var likeStatus = true;
                        console.log('IMAGE = ' + JSON.stringify(images));
                        console.log('userId = ' + user.userId);
                        console.log('targetId = ' + req.session.targetId);
                        console.log(req.protocol + ':\/\/' + req.get('Host') + '/profile/' + req.params.username);
                        res.render('profile', {
                            layout: 'dashboard',
                            user: user,
                            images: images,
                            mainpix: mainpix,
                            like: likeStatus,
                            tags: tags,
                            currentUser: req.session.currentUser.username,
                            url: url
                        });
                    })
                })
            })
        })
        } else {
            res.render('notFound', { layout: 'dashboard', error: "This page doesn't exist" });
        }
    });
})

router.post('/:username/like', function(req, res) {
    console.log('The person I like is userId/targetId = ' + req.session.targetId);
    Like.checkLikeMe(req.session.targetId, req.session.userId, function(rows) {
            Like.create(req.session.userId, req.session.targetId);
            res.end();
        })
})

router.post('/:username/unlike', function(req, res) {
    Like.delete(req.session.userId, req.session.targetId);
})

router.post('/:username/block', function(req, res) {
    Block.create(req.session.userId, req.session.targetId);
    res.end();
})

router.post('/:username/unblock', function(req, res) {
    Block.delete(req.session.userId, req.session.targetId);
    res.end();
})

router.post('/:username/report', function(req, res) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: process.env.TRANSPORT_EMAIL,
          pass: process.env.TRANSPORT_PASSWORD,
        }
      });

      const html = ` Hi Admin,
                    <br/>
                    The user ${req.session.currentUser.username} has reported the following user as a FAKE PROFILE:
                    <br/>
                    Reported user: <b>${req.params.username}</b>
                    <br/>
                    See this filthy fake user on the following page:<br/>
                    ${req.protocol + ':\/\/' + req.get('Host') + '/profile/' + req.params.username}
                    <br/>`;

      let mailOptions = {
        from: 'From Matcha Team <Matcha@gmail.com>', // admin
        to: process.env.TRANSPORT_EMAIL, // admin
        subject: 'User reported as FAKE',
        html: html,
      };
      transporter.sendMail(mailOptions, (err, info) => {
        if (err)
          return console.log(err);
        else {
          console.log(user.email)
          console.log('Mail is sent!!!');
          res.end();
        }
      })
})


module.exports = router;