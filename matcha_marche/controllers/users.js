var express = require('express');
var router = express.Router();
var session = require('express-session');
var User = require('../models/user');
var Image = require('../models/image');
var Tag = require('../models/tag');
var fs = require('fs-extra');
var fileUpload = require('express-fileupload');
var checkProfil = require('../middlewares/checkProfil');
var userhelper = require('../helpers/userhelpers')
var async = require('async');

router.get('/home', function (req, res) {
	if (req.session.userId == undefined) {
		res.redirect('/auth');
	}
	else {
		res.redirect('/users/gallery');
	}
});

router.get('/dashboard', function (req, res) {
	if (req.session.userId != undefined) {
		Image.checkMainPic(req.session.userId, function(mainPic) {
			if (mainPic.length == 0) {
				req.session.mainPic = 0;
			} else {
				req.session.mainPic = 1;
			}
		})
		User.getAllById(req.session.userId, function (rows) {
			if (userhelper.checkEmptyField(rows[0]) == false) {
				req.session.profilCompleted = 0;
			} else {
				req.session.profilCompleted = 1;
			}
		})
		var error = req.session.error;
		delete req.session.error;
		Tag.getMine(req.session.userId, function (userTags) {
			Tag.getAll(function (rows) {
				User.getById(req.session.userId, function (user) {
					req.body.firstName = user.firstName;
					req.body.lastName = user.lastName;
					req.body.age = user.age;
					req.body.username = user.username;
					req.body.email = user.email;
					req.body.gender = user.gender;
					req.body.bio = user.bio;
					req.body.sexpref = user.sexpref;
					// req.body.geoloc = user.geoloc;
					// req.body.position = user.position;

					// CONVERT TAGS ROWS IN ARRAY OF STRING
					var tags = userTags.map(function (elem) {
						return (elem['name']);
					})

					// SAVE USER OBJECT IN SESSION
					req.session.currentUser = user;
					req.session.currentUser.tags = tags.join();

					User.getMainPic(req.session.userId, user, function (err) {
						Image.getAllByUserId(req.session.userId, function (images) {
						res.render('dashboard', {
							layout: 'dashboard',
							error: error,
							images: images,
							data: { user },
							tags: { rows },
							userTags: userTags,
							currentUser: user.username,
						});
					})
				})
				})
				})
			})
	} else {
		res.redirect('/auth');
	}
})

router.post('/dashboard', function (req, res) {
	if (res.locals.error != undefined) {
		req.session.error = res.locals.error;
		console.log('erreur' + res.locals.error)
		res.redirect('/users/dashboard');
		return;
	}

	let profil = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		age: req.body.age,
		email: req.body.email,
		gender: req.body.gender,
		sexpref: req.body.sexpref,
		bio: req.body.bio,
		// geoloc: req.body.geoloc,
		// position: req.body.position
	}
	User.save(profil, req.session.userId);
	Tag.deleteMine(req.session.userId);
	let tags = req.body.tags.split(',');
	async.eachSeries(tags, function (elem, callback) {
		Tag.create(elem, function (result) {
			if (result == false)
				console.log(elem + ' already exist.');
			Tag.getId(elem, function (row) {
				Tag.relate(req.session.userId, row.tagId);
				callback(false);
			})
		})
	},
		function (err) {
			req.session.profilCompleted = 1;
			res.redirect('/users/dashboard');
		})
})

// PICTURE GALLERY
router.get('/gallery', function (req, res) {
	if (req.session.userId == undefined) {
		res.redirect('/auth');
		return;
	}
	Image.checkMainPic(req.session.userId, function (mainPix) {
		if (mainPix.length == 0) {
			req.session.mainPic = 0;
		} else {
			req.session.mainPic = 1;
		}
	})
	let err = req.session.error;
	delete req.session.error;
	Image.getAll(req.session.userId, function (rows) {
		if (rows.length > 0) {
			res.locals.images = rows;
		}
		res.render('gallery', { 
			layout: 'dashboard',
			error: err, 
			currentUser: req.session.currentUser.username 
		});
	})
});

router.post('/gallery', function (req, res) {
	User.getById(req.session.userId, function (user) {
		var file = req.files.file;
		if (file == undefined || (file.mimetype != 'image/jpeg' && file.mimetype != 'image/png')) {
			req.session.error = 'The picture has an invalid format, please try again or choose another one.';
			console.log('Invalid format');
		} else {
			let timestamp = new Date().getTime();
			let path = 'public/gallery/' + user.username + '/' + (user.username + '_' + timestamp);
			let imgName = '/static/gallery/' + user.username + '/' + (user.username + '_' + timestamp);
			file.mv(path, function (err) {
				if (err) req.session.error = 'Error while saving in local';
				else {
					console.log('Image uploaded!');
				};
			})
			Image.create(req.session.userId, imgName, function (err) {
				if (err) console.log(err);
			});
		}
		res.redirect('/users/gallery')
	});
})

router.post('/gallery/remove', function (req, res) {
	if (req.body.hidden == "") {
		res.redirect('/users/gallery');
		return;
	}
	Image.delete(req.session.userId, req.body.hidden, function (err) {
		if (err) throw err;
	})
	res.redirect('/users/gallery');
})

router.post('/gallery/setmain', function (req, res) {
	if (req.body.hidden == "") {
		res.redirect('/users/gallery');
		return;
	}
	Image.resetProfil(req.session.userId, function(err) {
		if (err) throw err;
	})
	Image.setMain(req.session.userId, req.body.setmain, function (err) {
		if (err) throw err;
		req.session.mainPic = 1;
	})
	res.redirect('/users/dashboard');
})

module.exports = router;