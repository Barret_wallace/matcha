var checkTags = function(arr) {
	for (var i = 0; i < arr.length; i++) {
		if (/^#[a-za-z]+$/.test(arr[i]) == false) {
			return (false);
		}
	}
	return (true);
}

module.exports = function(req, res, next) {
	let error = undefined;

	let gender = req.body.gender;
	let first = req.body.firstName;
	let last = req.body.lastName;
	let age = req.body.age;
	let email = req.body.email;
	let sexPref = req.body.sexpref;
	let bio = req.body.bio;
	let tags = req.body.tags.split(',');

	if (gender.trim() == "" || first.trim() == "" || last.trim() == "" || age.trim() == "" | email.trim() == "" || sexPref.trim() == "" || bio.trim() == "") {
		error = "All fields are required";
	} else if (gender != "Man" && gender != "Woman" && sexPref != "Heterosexual" && sexPref != "Homosexual" && sexPref != "Bisexual") {
		error = "An error occured, please fix this before submit the form";
	} else if (/\s/.test(gender) || /\s/.test(first) || /\s/.test(last) || /\s/.test(email) || /\s/.test(sexPref)) {
		error = 'No space characters are authorized';
	}
	res.locals.error = error;
	next();
};
