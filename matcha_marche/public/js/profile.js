(function() {

    var user = document.querySelector('#currentUser').innerHTML;
    var likeContainer = document.querySelector('#like-container');
    var blockContainer = document.querySelector('#block-container');
    var reportContainer = document.querySelector('#report-container');

     // Ajax for liking user
    function likeUser(bool) {
        xhr = new XMLHttpRequest();
        var url = document.querySelector('#url').value;
        if (bool == true)
            url += '/like';
        else
            url += '/unlike';
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function(){
            if (xhr.readyState == 4 && xhr.status == 200) {
                console.log('Like/Dislike is working!');
            }
        }
        xhr.send();
    }

    var btnLike = document.querySelector('#btn-like');
    var heartIcon = document.querySelector('#i-like');
    likeContainer.addEventListener('click', function(ev) {
        if (btnLike.value == 'Like') {
            heartIcon.style.color = 'red';
            heartIcon.style.transition = '0.3s';
            btnLike.value = 'Unlike';
            likeUser(true);
        } else {
            heartIcon.style.color = 'grey';
            heartIcon.style.transition = '0.3s';
            btnLike.value = 'Like';
            likeUser(false);
        }
        ev.preventDefault();
    })

    // Ajax for blocking user
    function blockUser(bool) {
        xhr = new XMLHttpRequest();
        var url = document.querySelector('#url').value;
        if (bool == true)
            url += '/block';
        else
            url += '/unblock';
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function(){
            if (xhr.readyState == 4 && xhr.status == 200) {
                console.log('Block/Unblock is working!');
            }
        }
        xhr.send();
    }

    var btnBlock = document.querySelector('#btn-block');
    var blockIcon = document.querySelector('#i-block');
    blockContainer.addEventListener('click', function(ev) {
        if (btnBlock.value == 'Block') {
            blockIcon.style.color = 'black';
            btnBlock.value = 'Unblock';
            blockUser(true);
        } else {
            blockIcon.style.color = 'grey';
            btnBlock.value = 'Block';
            blockUser(false);
        }
        ev.preventDefault();
    })



})()