var express = require('express');
var router = express.Router();
var session = require('express-session');
var User = require('../models/user');
var Tag = require('../models/tag');
var fs = require('fs-extra');
var fileUpload = require('express-fileupload');
var async = require('async');
var geolib = require('geolib');
var userhelper = require('../helpers/userhelpers');

exports.countCommonsTags = function(arr1, arr2) {
	var nbTags = 0;
	if (arr1.length > arr2.length) {
		var tmp;
		tmp = arr1;
		arr1 = arr2;
		arr2 = tmp;
	}
	for (var i = 0; i < arr2.length; i++) {
		for (var j = 0; j < arr1.length; j++) {
			if (arr2[i] == arr1[j]) {
				nbTags++;
			}
		}
		j = 0;
	}
    return (nbTags);
}

exports.checkEmptyField = function(obj) {
    for (var i in obj) {
        if (obj[i] == "" || obj[i] == "NULL" || obj[i] == undefined)
            return (false);
    }
    return (true);
}
